package com.citi.project.portfolio.model;

/**
 * This enum class limits what can be entered for currency to USD, GBP, EUR, JPY, or CNY.
 */
public enum Currency {

    USD, GBP, EUR, JPY, CNY
    
}
