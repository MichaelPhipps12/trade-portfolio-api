package com.citi.project.portfolio.model;

/**
 * This class limits the trade state to BUY, SELL or HOLD.
 */
public enum PortfolioTradeState {

    BUY, SELL, HOLD
    
}
