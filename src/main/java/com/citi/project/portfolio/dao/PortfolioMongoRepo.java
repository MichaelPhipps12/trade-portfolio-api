package com.citi.project.portfolio.dao;

import com.citi.project.portfolio.model.Portfolio;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface PortfolioMongoRepo extends MongoRepository<Portfolio, String> {
    
    //List<Trade> findByAddress(String address);
}