package com.citi.project.portfolio.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.citi.project.portfolio.model.Currency;
import com.citi.project.portfolio.model.Portfolio;
import com.citi.project.portfolio.model.PortfolioTradeState;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PortfolioControllerTest {
    
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_save_findAll() {
        Portfolio testPortfolio = new Portfolio();

        testPortfolio.setStockTicker("AAPL");
        testPortfolio.setStockQuantity(500);
        testPortfolio.setTradeState(PortfolioTradeState.BUY);
        testPortfolio.setCurrency(Currency.EUR);
        testPortfolio.setAmount(1500.00);

        ResponseEntity<Portfolio> response = restTemplate.postForEntity("/v1/portfolio",
                                                                       testPortfolio,
                                                                       Portfolio.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);

        ResponseEntity<List<Portfolio>> findAllResponse = restTemplate.exchange(
                                                        "/v1/portfolio",
                                                        HttpMethod.GET,
                                                        null,
                                                        new ParameterizedTypeReference<List<Portfolio>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
    }
}
